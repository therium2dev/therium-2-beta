uint64 global_time; // in ms

// Called by level.cpp at start of level
void Init(string str) {

}

// This script has no need of input focus
bool HasFocus(){
    return false;
}

// This script has no GUI elements
void DrawGUI() {
}

void Update() {
		int num_chars = 0;
		MovementObject@ char = ReadCharacter(num_chars);
		vec3 character_point(char.position.x-RangedRandomFloat(-15.0f,15.0f),char.position.y+20,char.position.z-RangedRandomFloat(-15.0f,15.0f));
		MakeParticle("Data/Custom/Gyrth/snow/Particles/snow.xml",character_point,
				vec3(RangedRandomFloat(-2.0f,2.0f),RangedRandomFloat(-5.0f,-15.0f),RangedRandomFloat(-2.0f,2.0f)));
		MakeParticle("Data/Custom/Gyrth/snow/Particles/snow.xml",character_point,
				vec3(RangedRandomFloat(-2.0f,2.0f),RangedRandomFloat(-5.0f,-15.0f),RangedRandomFloat(-2.0f,2.0f)));
}