#name "Intro"
#participants 2
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
say 1 "Ghost" "..."
say 1 "Ghost" "...[wait 0.3]hmmm..."
say 1 "Ghost" "It seems I'm stuck in the middle of nowhere."
say 1 "Ghost" "Something tells me that loading this level file wasn't a good idea."
