#name "Note-Crete"
#participants 2
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
say 1 "Ghost" "An analysis on the spirit gods of the world -- [wait 0.3]assuming they're real."
say 2 "Note" "The tales say that crete originates from the spirits' excess energy as they twist the earth into their desired shape, [wait 0.3]perform tasks or affect sentients (cats, [wait 0.3]rabbits, [wait 0.3]dogs and wolves)."
say 2 "Note" "The more spirit energy there is in a specific area, [wait 0.3]the more excess spirit energy has been focused in that spot."
say 2 "Note" "Therefore, [wait 0.3]it's reasonable to assume that the more crete in a specific area, [wait 0.3]the closer it is to the god's graveyard - [wait 0.3]the Therium."
