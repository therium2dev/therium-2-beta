#name "Note-Fire"
#participants 1

say 0 "Ghost" "An ancient note. [wait 0.3]Can barely read it."
say 0 "Note" "Day by day, [wait 0.3]the demon kills us. [wait 0.5]We have tried everything next to fire, [wait 0.3]and even that has been discussed."
say 0 "Note" "I don't want to see our forest burn. [wait 0.5]But it's possible we have to set it on fire just to flush him out."
say 0 "Note" He's impossibly strong and agile. [wait 0.5]This is nothing like a miscreant, [wait 0.3]let alone the grandest wolf in the country."
say 0 "Note" "[wait 0.3]God spare mercy."
