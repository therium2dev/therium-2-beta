#name "Note-JairoP"
#participants 1

say 0 "Ghost" "It's a note. [wait 0.5]Torn, [wait 0.3]yellow [wait 0.1]and who knows how old."
say 0 "Note" "Every single day, [wait 0.3]now. [wait 0.5]It was horrifying to think it would happen in the very first place, [wait 0.3]but this frequency is beyond coincidence."
say 0 "Note" "Everyone's terrified. [wait 0.3]I know I am. [wait 0.5]Whoever is doing this to us needs to die before it gets out of control."
say 0 "Note" "All the talk about sightings out in the forest is only making us more paranoid every single day."
say 0 "Note" "The desert outpost seems to be safer - [wait 0.5]as if anything is. [wait 0.5]I just hope my family will be okay."
