#name "Note-Log"
#participants 1

say 1 "Ghost" "A log from the scribe. [wait 0.5]For tracking incoming and outgoing shipments."
say 0 "Note" "38th of Spurhelm: [wait 0.3]Incoming four wheelbarrows of food, [wait 0.5]including cabbages and carrots. [wait 0.5]Four drums of drinking water. [wait 0.5]Received from the farms. [wait 0.5]Approved."
say 0 "Note" "39th of Spurhelm: [wait 0.3]No activity to log."
say 0 "Note" "40th of Spurhelm: [wait 0.3]Incoming squad of three rabbit bounty hunters from the Cinderbreathe City. [wait 0.5]Here to contact a citizen, [wait 0.3]Bijou. [wait 0.5]Improper identification. [wait 0.5]Swiftly denied."
say 0 "Note" "41st of Spurhelm: [wait 0.3]Outgoing carriage of timber and firewood for the Cinderbreathe City's Hearthsbreath celebration. [wait 0.5]Incoming carriage of food and rations, [wait 0.3]smaller than usual."
say 0 "Note" "42nd of Spurhelm: [wait 0.3]Incoming squad of Cinderbreathe missionaries to warn about the increasing terrorist activity on the colonial grounds. [wait 0.5]Approved. [wait 0.5]Incoming carriage of weapons, [wait 0.5]including glaives, [wait 0.5]broadswords and spears. [wait 0.5]Twenty-two in total. [wait 0.5]Approved."
say 0 "Note" "43rd of Spurhelm: [wait 0.3]Incoming multiple squadrons of unknown rabbits, [wait 0.3]armed and hostile. [wait 0.5]Gate is shut tight. [wait 0.5]Will finish log later, [wait 0.3]I have to assist."
