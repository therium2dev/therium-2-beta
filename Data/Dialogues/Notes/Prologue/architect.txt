#name "Note-Architect"
#participants 1

say 0 "Ghost" "A somewhat recent note. [wait 0.5]Must be written by an architect."
say 0 "Note" "Whoever said this prison was a good idea can be fed to the wolves."
say 0 "Note" "Not only are the city prisons already functional in every way, [wait 0.3]but the placement is just terrible."
say 0 "Note" "I swear, [wait 0.3]this will be used as a warehouse once Wick hits. [wait 0.5]Even then, the cold will blow in and freeze anything worth keeping in this rookie mistake called a building."
say 0 "Note" "Whatever this one prisoner's here for, [wait 0.3]it better be important."
