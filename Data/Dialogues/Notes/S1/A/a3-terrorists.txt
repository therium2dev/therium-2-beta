#name "Note-Terrorists"
#participants 1
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
say 1 "Ghost" "A note from one of the commanding officers."
say 2 "Note" "39th of Spurhelm, [wait 0.3]671. [wait 0.5]These are directions for the squad commander's eyes only.
say 2 "Note" "Terrorist activity in the colonial grounds is steadily increasing."
say 2 "Note" "Round up half of your remaining men and leave the campgrounds to search for any suspicious persons. [wait 0.5]You should know where to direct your men."
say 2 "Note" "Keep paranoia down."
