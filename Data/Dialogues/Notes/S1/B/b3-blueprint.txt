#name "Note-Blueprint"
#participants 1

say 0 "Ghost" "I can barely make heads or tails of this -- [wait 0.3]I'm no alchemist. [wait 0.5]Not a blind one, either."
say 0 "Ghost" "But it says somewhere on here that when complete, [wait 0.3]you could stow away enough explosive force to destroy a tower in just one satchel."
say 0 "Ghost" "Must be based off Arliss technology..."
