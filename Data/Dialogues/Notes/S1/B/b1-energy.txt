#name "Note-Energy"
#participants 2
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
say 1 "Ghost" "A personal journal from a guard -- [wait 0.3]pages open, [wait 0.3]quite careless..."
say 2 "Note" "Being in here is a strange experience. [wait 0.5]From first glance, [wait 0.3]these colony grounds are nothing special."
say 2 "Note" "Unpredictable when it comes to biome, [wait 0.3]sure, [wait 0.3]but ultimately an average stretch of land."
say 2 "Note" "Yet something within me is stirring. [wait 0.5]Sometimes, [wait 0.3]I feel as if a stiff breeze flows through me, [wait 0.3]and my fur stands on end."
say 2 "Note" "I'm no poet. [wait 0.5]In fact, [wait 0.3]I hate the idea of even imitating one.
say 2 "Note" "But being here makes me want to run away and protect my soul. [wait 0.5]It's hard to explain."
