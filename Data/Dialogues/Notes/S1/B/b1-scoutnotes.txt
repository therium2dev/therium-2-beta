#name "Note-Scout"
#participants 1

say 0 "Ghost" "A set of notes from a Cinderbreathe scout..."
say 0 "Note" "These colony grounds are so strange... [wait 0.3]they've more variety in terrain than the entire continent next to us."
say 0 "Note" "Forest, [wait 0.3]desert, [wait 0.3]mountain, [wait 0.3]tundra... [wait 0.5]all but swamp I've seen. [wait 0.5]It doesn't feel natural, [wait 0.3]but then again, [wait 0.3]nothing here does."
say 0 "Note" "Every major hill we come across usually has a host of those strange stone lanterns. [wait 0.5]I can't help but feel like I'm being watched."
say 0 "Note" "We're in the stretch of desert right now. [wait 0.5]The ancient ruins make for good landmarks, [wait 0.3]but damn it if they aren't dusty."
say 0 "Note" "Just sleeping by these things, [wait 0.3]I have to deal with wiping away the sand... [wait 0.5]probably nowhere near as bad as Ouran, [wait 0.3]though."
