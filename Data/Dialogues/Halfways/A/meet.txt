#name "Meet"
#participants 4
set_character_pos 4 -192.699 -59.1544 -2.63702 -62
send_character_message 4 "set_animation \"Data\Custom\timbles\therium-2\Animations\interestingsit.anm\""
send_character_message 4 "set_eye_dir -192.862 -59.8497 -1.55583 1"
send_character_message 4 "set_torso_target -192.363 -60.0845 -1.89744 1"
send_character_message 4 "set_head_target -192.358 -58.7626 -0.727036 1"
set_character_pos 3 -190.678 -59.3877 -0.800145 130
send_character_message 3 "set_eye_dir -192.55 -58.8243 -2.56215 1"
send_character_message 3 "set_torso_target -191.462 -59.2662 -1.82172 1"
send_character_message 3 "set_head_target -192.097 -58.8838 -2.21172 1"
set_character_pos 2 -192.442 -59.2815 0.00727987 102
send_character_message 2 "set_eye_dir -192.704 -58.8235 -2.4243 1"
send_character_message 2 "set_torso_target -192.353 -59.2062 -0.809328 1"
send_character_message 2 "set_head_target -192.686 -58.9282 -1.92245 1"
set_character_pos 1 -174.433 -55.9024 9.97335 158
send_character_message 1 "set_animation \"Data\Animations\r_crouch.anm\""
send_character_message 1 "set_eye_dir -187.562 -58.5753 0.95401 1"
send_character_message 1 "set_head_target -186.914 -58.5363 2.14442 1"
set_cam -189.177 -59.0948 5.46747 0 21.82 0 23.7617
say 2 "???" "Greetings. [wait 0.3]Are you lost?"
send_character_message 4 "set_eye_dir -192.557 -58.592 -0.364457 1"
send_character_message 4 "set_torso_target -192.4 -60.4867 -1.8652 1"
send_character_message 4 "set_head_target -192.356 -58.5602 -0.708863 1"
set_cam -191.006 -59.0073 -2.26155 0 79.5 0 23.7617
say 4 "Jairo" "No. [wait 0.3]I'm just wandering through these parts, [wait 0.3]just decided this'd be a good spot to rest my knees."
send_character_message 4 "set_head_target -192.356 -58.5609 -0.709693 1"
send_character_message 2 "set_head_target -192.686 -58.9282 -1.92245 1"
send_character_message 3 "set_head_target -192.097 -58.8838 -2.21172 1"
send_character_message 3 "set_torso_target -191.462 -59.2662 -1.82172 1"
set_character_pos 3 -190.678 -59.3877 -0.800145 130
send_character_message 2 "set_torso_target -192.542 -59.4217 -0.798874 1"
set_cam -194.749 -58.786 -0.389234 0 -90.97 0 23.7617
say 2 "???" "If you could help us out for a bit..."
set_cam -68.1106 -5.90611 14.4646 0 -1.65 0 23.7617
send_character_message 2 "set_torso_target -192.542 -59.4217 -0.798874 1"
send_character_message 4 "set_head_target -192.356 -58.5609 -0.709693 1"
send_character_message 3 "set_head_target -192.097 -58.8838 -2.21172 1"
send_character_message 3 "set_torso_target -191.462 -59.2662 -1.82172 1"
set_character_pos 3 -190.678 -59.3877 -0.800145 130
send_character_message 2 "set_head_target -192.704 -58.789 -2.21919 1"
say 2 "???" "Have you seen this person?"
set_cam -194.395 -58.7164 0.0551443 0 -81.32 0 23.7617
send_character_message 3 "set_torso_target -191.439 -58.9581 -1.67032 1"
send_character_message 3 "set_head_target -192.382 -58.5998 -2.61616 1"
send_character_message 2 "set_head_target -191.762 -58.5766 -0.994798 1"
say 3 "????" "Mostly average height, [wait 0.3]somewhat skinny build..."
send_character_message 3 "set_torso_target -191.502 -59.2026 -1.58817 1"
send_character_message 2 "set_head_target -192.807 -58.8377 -2.27283 1"
say 2 "???" "White fur, [wait 0.3]blue hoodie, [wait 0.3]yellow flairs?"
send_character_message 3 "set_torso_target -191.434 -58.7523 -1.52706 1"
set_character_pos 3 -190.678 -59.4446 -0.800145 130
send_character_message 3 "set_animation \"Data\Animations\r_dialogue_handhips.anm\""
say 3 "????" "His face is a bit on the chubbier side, [wait 0.3]too."
send_character_message 4 "set_head_target -190.845 -58.7891 -1.22116 1"
set_cam -191.671 -59.0558 -0.820567 0 27.52 0 23.7617
say 4 "Jairo" "I haven't seen anyone fitting that description."
send_character_message 2 "set_head_target -192.548 -58.7188 -2.28732 1"
send_character_message 3 "set_head_target -192.507 -58.8141 -2.6568 1"
send_character_message 2 "set_torso_target -192.422 -59.1004 -0.924729 1"
send_character_message 3 "set_torso_target -191.251 -58.8672 -1.83463 1"
send_character_message 3 "set_animation \"Data\Animations\r_idle.anm\""
set_cam -189.515 -58.6478 -2.24207 0 134.58 0 23.7617
say 3 "????" "How about this certain other person..."
send_character_message 2 "set_animation \"Data\Animations\r_idle.anm\""
send_character_message 3 "set_head_target -192.663 -58.5411 -2.5077 1"
send_character_message 2 "set_head_target -191.461 -58.4133 -1.36056 1"
say 3 "????" "Tall, [wait 0.3]muscular, [wait 0.3]deep brown fur..."
send_character_message 3 "set_head_target -192.598 -58.8167 -2.41095 1"
send_character_message 2 "set_head_target -192.619 -59.1023 -2.4054 1"
say 3 "????" "Two red stripes under his left eye..."
send_character_message 2 "set_head_target -190.106 -57.9685 0.856678 1"
send_character_message 2 "set_torso_target -192.142 -59.1958 -0.784086 1"
set_cam -195.992 -59.2681 -2.74137 4.64 -118.12 -0.63 23.7617
say 2 "???" "Blind in his right eye, [wait 0.3]scars across his muzzle..."
send_character_message 3 "set_torso_target -191.886 -58.7497 -1.47827 1"
send_character_message 3 "set_head_target -192.501 -58.605 -0.579472 1"
send_character_message 2 "set_animation \"Data\Animations\r_actionidle.anm\""
say 2 "???" "...what are you looking at?"
send_character_message 1 "set_animation \"Data\Animations\r_actionidlecrouch.anm\""
set_character_pos 3 -190.678 -59.4446 -0.800145 174
send_character_message 3 "set_torso_target -191.439 -59.7316 -0.668622 1"
send_character_message 3 "set_head_target -188.021 -57.9937 1.39136 1"
send_character_message 2 "set_head_target -189.375 -57.9333 1.76659 1"
send_character_message 3 "set_animation \"Data\Animations\r_actionidle.anm\""
say 3 "????" "Hey! [wait 0.3]That's him! [wait 0.3]The new one!"
set_cam -172.634 -55.3701 10.3913 -10.98 65.18 0.64 44.3879
say 2 "???" "Get him before he runs!"
send_level_message "make_all_aware"
