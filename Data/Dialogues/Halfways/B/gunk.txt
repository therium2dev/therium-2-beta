#name "Gunk"
#participants 2
set_character_pos 2 -92.2435 3.61111 150.211 132
send_character_message 2 "set_animation \"Data\Custom\timbles\therium-2\Animations\Gunk\gunksitcrossedlegs.anm\""
send_character_message 2 "set_eye_dir -94.5593 4.16351 147.873 1"
send_character_message 2 "set_head_target -94.2038 4.0505 147.5 1"
set_character_pos 1 -94.7159 3.3621 147.738 -42
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 1 "set_eye_dir -92.3682 3.86853 150.08 1"
send_character_message 1 "set_torso_target -94.293 3.35356 149.251 0"
send_character_message 1 "set_head_target -92.806 4.49607 149.425 1"
set_cam -93.1246 3.94681 147.623 0 100.12 0 33.8672
say 1 "Ghost" "Hey, [wait 0.3]are you alright?"
send_character_message 2 "set_eye_dir -94.5593 4.16351 147.873 0.792059"
send_character_message 2 "set_head_target -94.6148 4.29284 147.847 1"
set_cam -93.0531 4.04445 147.788 -8.13 -162.09 0 21.1377
say 2 "Gunk" "I heard a scuffle up there."
send_character_message 2 "set_head_target -94.5622 3.95639 147.852 1"
send_character_message 2 "set_eye_dir -94.5593 4.16351 147.873 1"
say 2 "Gunk" "More important question is, [wait 0.3]are you alright?"
send_character_message 1 "set_head_target -92.2795 3.68919 150.073 1"
send_character_message 1 "set_animation \"Data\Animations\r_crouch.anm\""
set_cam -92.6158 3.27264 148.206 8.45 80.84 0.16 19.0183
say 1 "Ghost" "I'm perfectly fine, [wait 0.3]thanks."
send_character_message 2 "set_eye_dir -92.5854 3.8374 150.509 1"
send_character_message 2 "set_head_target -92.7235 3.64149 150.387 1"
set_cam -93.7969 3.7745 149.893 -0.16 -102.7 -0.15 25.5022
say 1 "Ghost" "What happened to your arm?"
send_character_message 2 "set_head_target -92.7989 3.69115 150.343 1"
send_character_message 2 "set_eye_dir -94.4649 3.82168 148.042 1"
say 2 "Gunk" "This is not a new wound, [wait 0.3]trust me. [wait 0.3]I've been like this for years."
send_character_message 2 "set_head_target -94.381 4.43207 148.052 1"
say 2 "Gunk" "My name is Gunk. [wait 0.3]What's yours?"
send_character_message 1 "set_head_target -92.3448 3.95408 150.165 1"
send_character_message 1 "set_animation \"Data\Animations\r_actionidlecrouch.anm\""
set_cam -95.2748 3.5404 149.653 0.12 -23.23 -0.18 25.5022
say 1 "Ghost" "Call me Ghost. [wait 0.3]Let me get you out of there..."
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_armcross.anm\""
send_character_message 1 "set_eye_dir -90.0959 4.18173 152.795 1"
send_character_message 2 "set_torso_target -90.9681 3.76195 153.291 0"
send_character_message 2 "set_eye_dir -92.1399 4.17038 154.366 1"
send_character_message 1 "set_torso_target -91.5409 3.81844 154.019 0"
send_character_message 2 "set_head_target -92.089 4.16599 154.281 1"
set_character_pos 1 -92.1938 3.52506 154.37 35
send_character_message 1 "set_head_target -90.1032 4.1707 152.83 1"
send_character_message 2 "set_animation \"Data\Custom\timbles\therium-2\Animations\Gunk\gunkactionidle.anm\""
set_character_pos 2 -90.0325 3.38007 152.74 -108
set_cam -74.1699 -0.308035 124.813 0.13 87.06 0.17 25.5022
say 0 "" "..."
send_character_message 1 "set_head_target -90.0093 3.93132 153.001 1"
set_cam -90.8915 4.09212 154.371 0.13 87.06 0.17 25.5022
say 1 "Ghost" "What were you doing in there?"
set_cam -91.7075 3.56345 153.51 14.21 -66.39 -0.16 25.5022
send_character_message 2 "set_head_target -92.2073 4.04642 154.347 1"
send_character_message 1 "set_head_target -90.1776 4.15263 152.847 1"
say 2 "Gunk" "Well, [wait 0.3]it's not like I'd lock myself in there willingly."
send_character_message 2 "set_head_target -91.9741 3.87211 154.31 1"
say 2 "Gunk" "I've been there since sunrise, [wait 0.3]just waiting for someone like you to come along."
send_character_message 1 "set_head_target -90.0865 3.90351 153.152 1"
set_cam -90.9201 4.07354 154.327 -0.05 90.1 0.19 25.5022
say 1 "Ghost" "Who were they?"
set_cam -90.2705 4.07844 154.229 0.2 -8.65 0.02 25.5022
say 2 "Gunk" "I have no idea. [wait 0.3]I don't even think they're real people anymore."
send_character_message 2 "set_torso_target -90.4275 3.10374 153.569 1"
say 2 "Gunk" "It's like they're walking, [wait 0.3]breathing corpses."
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_thoughtful.anm\""
send_character_message 1 "set_head_target -90.2043 3.9895 152.881 1"
set_cam -91.7249 4.09716 153.23 -0.2 160.57 0.02 25.5022
say 1 "Ghost" "And they pack one hell of a punch."
send_character_message 2 "set_torso_target -91.0108 4.32732 153.92 1"
set_cam -91.2186 4.06987 153.918 0.17 -46.79 -0.11 25.5022
say 2 "Gunk" "They overpowered me, [wait 0.3]and held me down. [wait 0.3]I'm not sure what they would've done to me. Thank you."
send_character_message 1 "set_torso_target -91.3885 3.42967 154.172 1"
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
set_cam -90.5884 3.99889 155.219 0.06 59.01 0.19 25.5015
say 1 "Ghost" "Don't mention it. [wait 0.3]We should probably get out of here."
send_character_message 1 "set_torso_target -91.3885 3.42967 154.172 0.988216"
set_character_pos 1 -92.1938 3.52501 154.37 35
send_character_message 2 "set_head_target -90.0689 4.27316 155.741 1"
send_character_message 2 "set_torso_target -90.9216 3.14726 154.144 1"
set_cam -88.6015 4.0713 152.209 -0.13 118.85 0.15 25.5015
say 2 "Gunk" "I'll be on my way, [wait 0.3]then..."
send_character_message 2 "set_head_target -91.9017 4.31397 154.278 1"
set_character_pos 1 -92.1938 3.34305 154.37 35
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\lanterntouch.anm\""
send_character_message 1 "set_torso_target -91.3885 3.42967 154.172 0"
say 1 "Ghost" "Hey, [wait 0.3]wait - [wait 0.5]do you know where the Cinderbreathe Empry is from here?"
send_character_message 2 "set_torso_target -90.9216 3.14726 154.144 0"
send_character_message 2 "set_head_target -92.0551 4.06697 154.661 1"
set_cam -91.6767 3.6278 153.341 15.85 -70.84 -0.18 25.5015
say 2 "Gunk" "I've no idea. [wait 0.3]They broke my compass."
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
set_cam -91.6766 3.62778 153.341 15.85 -70.84 -0.18 25.5015
send_character_message 2 "set_torso_target -91.2167 4.30091 153.862 1"
say 2 "Gunk" "Regardless, [wait 0.3]we have to get out of here, [wait 0.3]before more of them come."
send_character_message 1 "set_torso_target -91.3837 3.40392 154.066 1"
send_character_message 2 "set_torso_target -91.0075 3.25856 153.783 1"
set_cam -87.5591 3.80574 152.952 0.36 95.13 0.08 39.6954
say 1 "Ghost" "Right."
send_character_message 1 "set_head_target -90.2115 4.15793 152.771 1"
send_character_message 2 "set_head_target -91.8577 4.48077 153.918 1"
say 2 "Gunk" "If I see you again, [wait 0.3]I owe you."
send_character_message 1 "set_torso_target -91.2035 3.4064 154.412 1"
send_character_message 1 "set_head_target -79.6013 2.8743 169.965 1"
set_cam -93.0297 4.01598 152.79 -0.25 -143.99 0.27 23.3753
say 0 "Ghost" "..."
send_level_message "loadlevel Data/Levels/t2/s2/b.xml"
