set_character_pos 2 -55.9247 -7.06376 165.788 150
send_character_message 2 "set_animation \"Data/Animations/r_bigdogswordarmstance.anm\""
send_character_message 2 "set_head_target -57.1969 -6.28894 164.555 1"
set_character_pos 1 -63.9172 -7.01589 168.302 94
send_character_message 1 "set_animation \"Data/Animations/r_crouch_sad.anm\""
send_character_message 1 "set_head_target -62.7852 -6.7018 167.174 1"
set_cam -64.2534 -6.77182 165.754 0 -168.87 0 16.8612
#name "Meet"
#participants 2

say 0 "Ghost" "..."
set_cam -66.191 -6.7271 168.394 0 -78.87 0 16.8612
say 0 "Ghost" "..."
send_character_message 2 "set_head_target -56.727 -6.19659 164.044 1"
set_cam -60.048 -6.41406 164.504 0 -108.87 0 16.8612
say 0 "???" "..."
send_character_message 1 "set_head_target -65.3406 -6.59957 168.128 1"
set_character_pos 1 -64.1263 -7.01589 168.556 154
send_character_message 2 "set_eye_dir -61.3764 -6.57087 166.35 1"
send_character_message 2 "set_head_target -61.0315 -6.19659 165.715 1"
say 0 "???" "..."
send_character_message 1 "set_head_target -63.4873 -6.62854 167.52 1"
send_character_message 1 "set_animation \"Data/Animations/r_actionidlecrouch.anm\""
set_cam -67.465 -6.85059 168.704 0 -78.87 0 16.8612
say 0 "Ghost" "(Who the hell is that...?)"
send_character_message 1 "set_head_target -65.0816 -6.92515 167.924 1"
say 0 "Ghost" "[wait 0.5](What the hell is that?)"
send_character_message 2 "set_torso_target -55.44 -6.65013 164.77 1"
send_character_message 2 "set_head_target -55.1934 -6.28493 164.003 1"
set_character_pos 2 -55.9247 -7.06376 165.788 90
set_cam -52.2272 -6.55243 164.118 0 106.99 0 23.1027
say 0 "???" "..."
send_character_message 2 "set_head_target -55.0524 -6.28493 164.046 1"
send_character_message 2 "set_torso_target -55.4401 -6.65013 164.77 1"
set_cam -64.5265 -6.72835 170.77 0 -37.56 0 23.1027
send_character_message 1 "set_head_target -64.2422 -6.92515 163.247 1"
set_character_pos 1 -63.8288 -7.00481 168.052 124
say 0 "Ghost" "..."
send_character_message 2 "set_head_target -54.341 -6.28493 164.372 1"
send_character_message 2 "set_torso_target -55.7568 -6.65013 164.702 1"
set_cam -58.808 -6.47873 166.645 0 -67.56 0 23.1027
say 0 "Ghost" "..."
set_character_pos 1 -64.8659 -7.00481 170.7 124
say 0 "Ghost" "I don't want to go anywhere near that thing..."
