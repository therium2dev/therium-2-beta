#name "Save-ML"
#participants 2
set_dialogue_color 1 0 0.02 0.05
set_dialogue_voice 1 8
say 0 "????" "..."
say 0 "????" "...[wait 0.3]?"
say 1 "Ghost" "You're fine. [wait 0.5]They won't be bothering you."
say 2 "????" "Short-tails got what they deserved, [wait 0.3]hmm..."
say 2 "????" "Who the hell are you?"
say 1 "Ghost" "I'm just looking out for you. [wait 0.5]What happened?"
say 2 "????" "I'm dissenting from the Empire. [wait 0.5]Tired of being the little guy."
say 1 "Ghost" "Where are you going to go, [wait 0.3]then?"
say 2 "????" "I know a guy or two. [wait 0.5]I'm gonna get into the Arliss."
say 2 "????" "Even being a slave for life is better than sitting around with Cinderbreathe filth."
say 2 "????" "The Arliss Empire will prevail, [wait 0.5]and so will I."
say 1 "Ghost" "..."
