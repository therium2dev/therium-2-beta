set_character_pos 1 -115.018 -6.79781 167.34 30
send_character_message 1 "set_animation \"Data/Animations/r_crouch_sad.anm\""
send_character_message 1 "set_head_target -113.802 -7.21318 166.564 1"
set_cam -111.951 -7.363 166.103 0 108.51 0 42.7851
#name "Corpses"
#participants 1

say 0 "Ghost" "...[wait 0.3]that'd be Ferdi."
send_character_message 1 "set_head_target -113.607 -6.71118 167.114 1"
set_cam -113.313 -6.90351 167.62 0 78.51 0 31.0413
say 0 "Ghost" "I shouldn't make too much noise..."
send_character_message 1 "set_head_target -113.297 -6.63211 167.368 1"
set_cam -113.704 -6.90191 166.93 0 108.51 0 31.0413
say 0 "Ghost" "Drika might be here, somewhere..."
