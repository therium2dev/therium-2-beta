#name "Intro"
#participants 2

say 0 "Ghost" "..."
say 2 "Jairo" "Ghost."
say 1 "Ghost" "Are there enemies nearby?"
say 2 "Jairo" "All over the place. [wait 0.5]They know you're going to travel through here. [wait 0.5]They just don't know how."
say 2 "Jairo" "Every single guard is armed with a weapon of some kind. [wait 0.4]Most are equipped with armour."
say 1 "Ghost" "Should I just find the lantern and get out...?"
say 2 "Jairo" "No. [wait 0.4]Not yet."
say 2 "Jairo" "You need to eliminate every single guard here. [wait 0.5]It doesn't matter how you do it."
say 2 "Jairo" "But when you're done with them, [wait 0.3]you're going to need to gather intel."
say 2 "Jairo" "Documents, [wait 0.2]papers, [wait 0.2]files -- [wait 0.5]anything you can find."
