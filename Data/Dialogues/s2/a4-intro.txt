#name "Intro"
#participants 2

say 0 "Ghost" "..."
say 0 "Ghost" "..."
say 0 "Ghost" "...[wait 0.5]where am I?"
say 0 "Ghost" "...[wait 0.5]oh, right.[wait 0.3] The creature by the lake..."
say 0 "Ghost" "I think I came here to sleep for the night."
say 0 "Ghost" "...[wait 0.5]where is this cave?"
say 0 "Ghost" "..."
say 0 "Ghost" "..."
say 0 "Ghost" "...[wait 0.5]at least I kept this lantern for myself."
say 0 "Ghost" "I've got to find my way out of here..."
