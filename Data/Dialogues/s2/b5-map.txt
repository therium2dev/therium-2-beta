#name "Map"
#participants 2

say 1 "Ghost" "Hello there. [wait 0.5]I was told you have a map of the nearby area?"
say 2 "Brandt" "Yeah, [wait 0.3]I overheard."
say 2 "Brandt" "Go ahead. [wait 0.5]We have a spare."
say 0 "Ghost" "..."
say 0 "Ghost" "...[wait 0.3]a map of the Therium. [wait 0.5]Just what I've needed."
say 0 "Ghost" "I can find my way back to the Empry with this... [wait 0.5]about time."
say 0 "Ghost" "But... [wait 0.3]maybe I should find Jairo."
