#name "Trap"
#participants 8
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
set_dialogue_color 2 1.0 0.0 0.0
set_dialogue_voice 2 6
say 1 "Ghost" "Hello... are you listening?"
say 1 "Ghost" "...this is a trap!"
say 3 "???" "..."
say 1 "Ghost" "...not more of these!"
say 2 "????" "Hey!"
say 1 "Ghost" "...what? Gunk?"
say 2 "Gunk" "I'll try to find a weapon for you -- fight them off in the meantime!"
say 1 "Ghost" "Alright, got it!"
say 1 "Ghost" "I'll just have to survive in the meantime..."
