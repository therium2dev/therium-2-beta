#name "Success"
#participants 3

say 0 "" "..."
say 1 "Ghost" "[wait 0.2]Jairo!"
say 1 "Ghost" "You can come out now!"
say 2 "Jairo" "Well done, [wait 0.15]Ghost."
say 2 "Jairo" "I'll carry one of them."
say 2 "Jairo" "The alive one, that is."
say 2 "Jairo" "You, [wait 0.2]carry the corpses."
say 2 "Jairo" "I'll show you where to stash them."
say 2 "Jairo" "[wait 0.1]He'll be in a hell of a shock when he wakes up."
say 0 "" "..."
send_level_message "loadlevel Data/Levels/t2/s2/a-2.xml"
