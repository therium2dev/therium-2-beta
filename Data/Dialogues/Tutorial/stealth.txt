#name "Stealth"
#participants 1
set_dialogue_color 1 0 0.02 0.05
set_dialogue_voice 1 8
say 1 "You" "...[wait 0.3]more people here..."
say 1 "You" "They'll hurt me. [wait 0.5]Have to be wary of them..."
say 1 "You" "...[wait 0.3]I might kill them."
