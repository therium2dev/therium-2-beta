#name "Intro"
#participants 2

say 1 "Ghost" "What am I here for now, [wait 0.3]Jairo?"
say 2 "Jairo" "You just have to make your way through."
say 2 "Jairo" "Eliminate any enemies as you see fit, [wait 0.3]though I'd advise you kill them all."
say 2 "Jairo" "You'll receive further instructions in the next gateway."
say 1 "Ghost" "Is there only one gateway here?"
say 2 "Jairo" "It's on the other side of this cavern."
say 2 "Jairo" "There's armed enemies all around, [wait 0.3]though. [wait 0.5]Watch yourself and be wary."
say 1 "Ghost" "Got it."