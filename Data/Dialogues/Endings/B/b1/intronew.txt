#name "Intro"
#participants 2
set_character_pos 2 237.946 36.7204 -134.177 -150
send_character_message 2 "set_animation \"Data\Animations\r_crouch_sad.anm\""
send_character_message 2 "set_head_target 215.423 26.4312 -114.83 1"
set_character_pos 1 215.261 25.7464 -114.621 40
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_shade.anm\""
send_character_message 1 "set_eye_dir 237.203 37.245 -133.72 1"
send_character_message 1 "set_torso_target 215.958 26.372 -115.052 1"
send_character_message 1 "set_head_target 237.739 37.5693 -133.967 1"
set_cam 214.362 25.9586 -113.268 12.72 -42.83 0 35.2887
say 1 "Ghost" "Jairo, finally! [wait 0.4]Where were you?"
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_handneck.anm\""
say 1 "Ghost" "I've been lost for a little while."
set_cam 237.339 36.9369 -132.227 0.08 -10.92 0.05 26.1866
say 2 "Jairo" "Ghost, [wait 0.5]what are you doing?"
send_character_message 1 "set_animation \"Data\Animations\r_idle.anm\""
set_cam 217.273 26.3269 -116.062 -0.09 126.91 0.02 26.1866
say 0 "Ghost" "..."
set_character_pos 2 237.946 36.7226 -134.177 -150
send_character_message 1 "set_head_target 237.741 37.5727 -133.966 1"
send_character_message 2 "set_torso_target 237.088 37.2673 -133.729 3.92497e-005"
send_character_message 2 "set_animation \"Data\Animations\r_actionidlecrouch.anm\""
set_cam 236.085 36.9691 -132.806 0.09 -53.09 -0.02 21.8795
say 2 "Jairo" "You're not supposed to be here. [wait 0.2]You can't be here."
set_character_pos 2 237.946 36.8301 -134.177 -150
set_cam 236.085 36.9691 -132.806 0.09 -53.09 -0.02 21.8795
send_character_message 1 "set_head_target 237.741 37.5727 -133.966 1"
send_character_message 2 "set_torso_target 237.088 37.2673 -133.729 3.92497e-005"
send_character_message 2 "set_animation \"Data\Animations\r_dialogue_handhips.anm\""
say 2 "Jairo" "You were supposed to follow the shrines marked with A, [wait 0.2]and kill the Cinderbreathe forces. [wait 0.4]Why are you here?"
send_character_message 2 "set_torso_target 237.088 37.2673 -133.729 3.92497e-005"
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_welcome.anm\""
send_character_message 1 "set_head_target 237.652 37.0765 -133.968 1"
set_cam 217.258 26.4214 -114.923 -0.07 96.91 0.06 21.8795
say 1 "Ghost" "But I got lost - [wait 0.5]I still don't know where I am!"
send_character_message 2 "set_torso_target 237.088 37.2673 -133.729 3.92497e-005"
set_cam 236.333 37.5005 -134.34 0.05 -98.94 -0.08 21.8795
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 2 "Jairo" "That's no excuse. [wait 0.3]Did you deliberately abandon the instructions?"
send_character_message 2 "set_animation \"Data\Animations\r_dialogue_armcross.anm\""
send_character_message 2 "set_torso_target 237.003 36.0915 -133.593 1"
say 2 "Jairo" "The path was clear. [wait 0.3]You were told where to go. [wait 0.3]It was all obvious!"
send_character_message 2 "set_torso_target 237.003 36.0915 -133.593 0.985167"
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_welcome.anm\""
set_cam 238.705 38.1239 -135.152 -21.08 136.87 -0.01 18.3775
say 1 "Ghost" "Jairo, [wait 0.2]stop!"
send_character_message 2 "set_torso_target 237.003 36.0915 -133.593 0"
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 0 "Jairo" "..."
send_character_message 1 "set_head_target 216.53 26.8014 -115.405 1"
send_character_message 2 "set_head_target 215.319 26.2647 -114.756 1"
send_character_message 1 "set_torso_target 215.958 26.372 -115.052 0"
set_cam 216.703 26.4381 -115.604 0.15 124.7 -0.04 18.3775
say 1 "Ghost" "I have no idea what I'm doing here."
send_character_message 1 "set_head_target 216.404 26.8695 -115.541 1"
say 1 "Ghost" "You expect me to just murder a couple dozen innocent people [wait 0.15]just because someone told me to?"
send_character_message 2 "set_animation \"Data\Animations\r_actionidle.anm\""
set_cam 216.703 26.4381 -115.604 0.11 94.7 -0.11 18.3775
send_character_message 1 "set_head_target 237.432 37.1221 -133.854 1"
say 1 "Ghost" "I could've broken out of that prison easily, [wait 0.2]anyways. [wait 0.4]The entire thing was practically a shanty!"
send_character_message 2 "set_animation \"Data\Animations\r_dialogue_handhips.anm\""
set_cam 214.021 25.8151 -113.119 18.32 -42.95 0.01 18.3775
say 2 "Jairo" "Ghost, [wait 0.2]stop."
send_character_message 1 "set_torso_target 215.958 26.372 -115.052 1"
say 1 "Ghost" "I won't stop until you give me some answers. [wait 0.3]And now."
send_character_message 1 "set_head_target 218.291 26.0497 -118.175 1"
set_character_pos 1 215.261 25.6913 -114.621 40
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_handhips.anm\""
set_cam 213.636 26.4092 -115.836 0.03 -122.62 -0.09 18.3775
say 1 "Ghost" "I don't know who you are. [wait 0.4]I don't know why you want the Cinderbreathe to die. [wait 0.4]I don't even know where we are."
send_character_message 1 "set_head_target 237.716 37.5337 -133.956 1"
say 1 "Ghost" "And who are those guys? [wait 0.4]The ones with the purple eyes and fur caked in dirt?"
send_character_message 1 "set_torso_target 215.955 26.3809 -115.052 1"
say 2 "Jairo" "Ghost..."
send_character_message 1 "set_animation \"Data\Animations\r_actionidlethreat.anm\""
send_character_message 1 "set_torso_target 215.994 26.6246 -114.989 1"
say 1 "Ghost" "If you don't give me answers now, [wait 0.5]I'll make you tell me."
send_character_message 2 "set_eye_dir 236.925 36.7942 -132.643 0"
send_character_message 2 "set_animation \"Data\Animations\r_idle.anm\""
set_cam 236.536 37.5999 -134.737 0.04 -113.2 -0.08 18.4163
say 2 "Jairo" "...fine."
send_character_message 2 "set_head_target 221.68 25.6778 -120.8 1"
say 2 "Jairo" "If a fight will shut you up..."
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 2 "set_eye_dir 215.631 26.5734 -114.676 1"
send_character_message 2 "set_head_target 215.587 26.2737 -114.844 1"
say 2 "Jairo" "Go through the shrine at the top of the stairs."
set_cam 217.202 26.3066 -116.027 -0.09 126.47 0.01 29.3072
say 1 "Ghost" "Doing your disappearing act again?"
send_character_message 1 "set_head_target 237.444 29.4984 -133.767 1"
say 1 "Ghost" "Fine."
send_character_message 1 "set_torso_target 216.227 26.0124 -115.046 1"
send_character_message 1 "set_eye_dir 216.979 25.8706 -115.043 1"
send_character_message 1 "set_head_target 218.041 26.1255 -115.206 1"
set_cam 215.687 26.3349 -116.108 -0.07 167.64 -0.05 29.3072
say 0 "Ghost" "..."
send_character_message 1 "set_torso_target 216.304 25.8338 -114.859 1"
send_character_message 1 "set_head_target 217.831 26.6837 -114.182 1"
say 1 "Ghost" "Hey, [wait 0.5]wait..."
send_character_message 1 "set_torso_target 216.304 25.8338 -114.856 1"
send_character_message 1 "set_head_target 220.875 29.0135 -111.495 1"
set_cam 212.998 25.3943 -116.578 16.3 -126.65 -0.09 29.3072
say 1 "Ghost" "I think I recognize this area, [wait 0.3]from my childhood... [wait 0.3]I can find my way back to the Empry from here..."
set_cam 215.536 25.1792 -116.697 27.56 -179.65 -0.09 29.3072
send_character_message 1 "set_torso_target 215.869 25.5292 -113.756 1"
send_character_message 1 "set_head_target 215.615 28.2143 -111.63 1"
say 1 "Ghost" "Then I can leave this 'Therium' behind me. [wait 0.3]Never come back. [wait 0.3]Forget about Jairo, [wait 0.3]forget about those creatures..."
send_character_message 1 "set_head_target 223.289 26.9012 -120.782 1"
set_cam 213.545 25.8474 -113.603 12.99 -53.9 -0.68 29.3072
send_character_message 1 "set_torso_target 216.057 25.4461 -114.949 1"
say 1 "Ghost" "But, [wait 0.3]at the same time... [wait 0.5]I should do something about Jairo..."
