#name "FoundSecure"
#participants 2

send_character_message 1 "set_eye_dir 140.368 -20.0078 93.5772 1"
send_character_message 1 "set_head_target 140.144 -19.7915 93.695 1"
set_cam 138.9 -19.8822 94.7966 0 -61.85 0 53.7656
set_character_pos 1 140.103 -20.3367 94.5835 80
say 1 "Ghost" "Bijou?"
send_character_message 1 "set_head_target 140.364 -19.674 93.734 1"
say 1 "Ghost" "Are you in here, [wait 0.3]Bijou!?"
send_character_message 1 "set_torso_target 139.988 -19.7203 93.3997 1"
send_character_message 1 "set_head_target 140.685 -19.674 93.3274 1"
set_character_pos 1 140.179 -20.2984 94.133 89
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 0 "Ghost" "..."
send_level_message "loadlevel Data/Levels/t2/e/b3-2.xml"
