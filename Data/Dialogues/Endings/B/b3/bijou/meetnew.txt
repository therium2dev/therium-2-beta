#name "Meet"
#participants 2
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
set_dialogue_color 2 0.7 0.7 0.89
set_dialogue_voice 2 11
say 1 "Ghost" "..."
say 1 "Ghost" "What the hell happened here?"
say 1 "Ghost" "Where did Bijou go?"
say 2 "Bijou" "[wait 0.4]D[wait0.2]on't move!"
say 1 "Ghost" "Bijou! [wait 0.3]What happened?"
say 2 "Bijou" "[wait 0.5]Amos!?"
say 2 "Bijou" "Amos, [wait 0.2]where have you been? [wait 0.3]Do you know what's happening?"
say 1 "Amos" "I was about to ask you that. [wait 0.2]But everything's fine."
say 1 "Amos" "As far as I can tell, [wait 0.3]they're all gone. [wait 0.4]The monsters, [wait 0.1]I mean."
say 2 "Bijou" "[wait 0.3]By who? [wait 0.3]The guards?"
say 1 "Amos" "No - [wait 0.2]the guards are all dead. [wait 0.2]As far as I can tell, [wait 0.4]none survived."
say 1 "Amos" "Your shoulder; [wait 0.3]are you fine?"
say 2 "Bijou" "I'm covered in blood, [wait 0.2]terrified out of my mind, [wait 0.4]and just had to kill some insane murderous monster thing."
say 2 "Bijou" "But, [wait 0.2]no, [wait 0.3]I'm fine, [wait 0.1]really."
say 1 "Amos" "[wait 0.3]Those bandages holding up fine?"
say 2 "Bijou" "Not at first. [wait 0.3]But I got the bleeding to stop. [wait 0.1]I'm fine."
say 2 "Bijou" "For once, [wait 0.3]don't worry about me. [wait 0.4]We have to get out of here."
