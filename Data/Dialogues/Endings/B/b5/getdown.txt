#name "Climb"
#participants 3
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
say 2 "???" "Hey!"
say 2 "???" "You really, [wait 0.3]really shouldn't do that! [wait 0.5]Do you know how old those rocks are?"
say 2 "???" "Get down from there unless you want to trip and break your own neck!"
