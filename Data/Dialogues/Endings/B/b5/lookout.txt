#name "Ruins"
#participants 2
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
say 1 "Amos" "...[wait 0.3]ruins of lookout point."
say 1 "Amos" "Impressive they're still holding up... [wait 0.3]considering they're more than five hundred years old."
