#name "NoKill"
#participants 1

say 1 "Ghost" "Come on, [wait 0.2]I know you're here!"
say 1 "Ghost" "I'm going to find you!"
say 1 "Ghost" "Just come out!"
say 1 "Ghost" "...[wait 0.4]damnit."
say 1 "Ghost" "[wait 0.1]That was too close."
send_level_message "loadlevel Data/Levels/t2/e/a3.xml"
