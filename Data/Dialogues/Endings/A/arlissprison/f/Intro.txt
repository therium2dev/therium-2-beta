#name "Intro"
#participants 1
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
say 1 "You" "..."
say 1 "You" "Alright, none of the guards saw me."
say 1 "You" "At least, none that are alive..."
say 1 "You" "Time to plant these charges."
say 1 "You" "...this is the first spot."
say 1 "You" "I just have to set it down slowly and carefully..."
