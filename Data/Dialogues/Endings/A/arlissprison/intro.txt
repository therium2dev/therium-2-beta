#name "Intro"
#participants 5
set_character_pos 3 -172.201 -51.4329 -139.842 -111
send_character_message 3 "set_animation \"Data\Animations\r_spearstance.anm\""
send_character_message 3 "set_torso_target -172.742 -51.4006 -138.497 0.0291983"
send_character_message 3 "set_head_target -172.095 -50.8258 -136.852 1"
set_character_pos 2 -170.133 -51.4009 -136.79 -163
send_character_message 2 "set_animation \"Data\Animations\r_dialogue_worriedpose_2.anm\""
send_character_message 2 "set_eye_dir -172.089 -51.1668 -136.226 1"
send_character_message 2 "set_torso_target -171.074 -51.6705 -136.74 0.937247"
send_character_message 2 "set_head_target -171.414 -50.9224 -136.714 1"
set_character_pos 1 -174.895 -51.3323 -106.324 -160
send_character_message 1 "set_eye_dir -176.429 -51.1303 -107.27 1"
send_character_message 1 "set_torso_target -176.124 -51.4305 -106.342 0"
send_character_message 1 "set_head_target -175.884 -50.9727 -106.147 0"
set_cam -333.282 -46.2411 -49.3413 0.94 -84.5 -0.03 57.7905
say 0 "" "..."
set_cam -173.107 -49.4585 -77.7581 1.22 -28.96 0 44.4642
say 0 "" "..."
set_cam -262.397 -51.6756 -98.9843 17.04 119.99 -0.25 41.2094
say 0 "" "..."
set_cam -230.981 -51.9707 -144.813 0.34 -96.23 0.05 10.9818
say 0 "" "..."
send_character_message 2 "set_head_target -171.472 -50.9645 -136.513 1"
set_cam -172.203 -50.7374 -138.099 0.32 -124.13 -0.11 17.7467
say 2 "???" "Something feels wrong."
send_character_message 3 "set_torso_target -172.411 -51.3747 -138.577 1"
send_character_message 3 "set_head_target -170.527 -50.6222 -137.301 1"
set_cam -170.135 -50.8699 -139.369 0 86.87 0 32.7239
say 3 "????" "It's your nerves talking again."
send_character_message 3 "set_torso_target -171.579 -51.5259 -138.54 1"
say 3 "????" "If the Kaicalk even think of rioting, [wait 0.3]we have them locked down."
send_character_message 2 "set_eye_dir -172.089 -51.1668 -136.226 1"
send_character_message 2 "set_torso_target -171.074 -51.6705 -136.74 0"
send_character_message 2 "set_head_target -171.662 -50.8237 -138.38 1"
set_cam -170.241 -50.8699 -138.349 0 172.21 0 32.7239
send_character_message 2 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 3 "set_head_target -170.385 -50.8542 -137.243 1"
send_character_message 3 "set_torso_target -171.579 -51.5259 -138.54 0"
say 3 "????" "There is nothing to worry about."
send_character_message 2 "set_torso_target -170.922 -51.5798 -136.459 1"
send_character_message 2 "set_head_target -171.514 -49.7188 -129.589 1"
say 2 "???" "Hey - [wait 0.5]did you hear that?"
send_character_message 2 "set_torso_target -170.947 -51.3067 -136.848 1"
send_character_message 2 "set_head_target -170.529 -50.7018 -131.769 1"
send_character_message 3 "set_torso_target -172.084 -51.487 -138.605 1"
set_cam -174.493 -50.9465 -140.991 0 -126.38 0 27.7676
say 3 "????" "I heard nothing."
send_character_message 2 "set_torso_target -170.892 -51.5079 -136.958 1"
send_character_message 2 "set_head_target -171.239 -50.7705 -138.463 1"
say 2 "???" "I could swear I heard a gasp over there... [wait 0.3]and maybe some fighting."
send_character_message 3 "set_torso_target -172.084 -51.487 -138.605 0"
send_character_message 3 "set_head_target -172.773 -50.0274 -135.589 1"
set_cam -173.551 -50.7366 -138.592 0 -52.26 0 27.7676
say 3 "????" "I'll check it out, [wait 0.3]if it really makes you feel better."
send_character_message 3 "set_eye_dir -170.258 -50.2555 -142.649 1"
set_character_pos 3 -176.145 -51.3747 -107.048 -31
send_character_message 3 "set_torso_target -170.947 -51.5662 -141.553 0"
send_character_message 3 "set_head_target -169.557 -49.9716 -141.138 0"
send_character_message 2 "set_eye_dir -172.089 -51.1668 -136.226 0.709453"
send_character_message 2 "set_head_target -172.601 -51.1887 -138.245 1"
send_character_message 2 "set_torso_target -171.148 -51.1717 -137.013 1"
send_character_message 2 "set_animation \"Data\Animations\r_idle.anm\""
set_character_pos 2 -170.133 -51.4013 -136.789 -163
set_cam -171.267 -50.7938 -138.488 0 -147.72 0 27.7676
say 2 "???" "Thanks, [wait 0.3]I..."
set_character_pos 1 -175.188 -51.4137 -107.154 157
set_character_pos 3 -176.158 -51.3753 -107.704 -31
set_cam -170.16 -50.7939 -138.583 0 175.12 0 23.1836
send_character_message 2 "set_head_target -170.792 -50.646 -133.841 1"
send_character_message 2 "set_torso_target -170.481 -51.0516 -135.894 1"
set_character_pos 2 -170.133 -51.4013 -136.789 -109
say 0 "???" "..."
set_character_pos 1 -174.835 -51.4137 -107.004 157
send_character_message 1 "set_eye_dir -178.15 -51.4962 -108.917 1"
send_character_message 1 "set_head_target -177.878 -51.5944 -109.407 1"
send_character_message 1 "set_animation \"Data\Animations\r_smallswordthrust.anm\""
send_character_message 3 "set_animation \"Data\Animations\r_blockfrontko.anm\""
send_character_message 2 "set_torso_target -170.481 -51.0516 -135.894 0"
send_character_message 2 "set_head_target -170.692 -50.5109 -133.601 1"
send_character_message 2 "set_animation \"Data\Animations\r_actionidle.anm\""
say 0 "???" "...!"
send_character_message 2 "set_head_target -170.692 -50.5108 -133.601 1"
send_character_message 2 "set_torso_target -170.498 -51.5123 -136.235 0.980621"
say 2 "???" "What was that!?"
send_character_message 1 "set_eye_dir -170.235 -50.6651 -136.339 1"
send_character_message 1 "set_torso_target -175.034 -51.5018 -114.627 0"
send_character_message 2 "set_torso_target -170.364 -51.3274 -136.315 1"
send_character_message 2 "set_head_target -172.988 -50.9995 -122.478 1"
send_character_message 1 "set_head_target -172.484 -50.8999 -125.239 1"
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\ghostsword_idle.anm\""
set_character_pos 1 -175.292 -51.4556 -113.442 68
send_character_message 2 "set_eye_dir -172.089 -51.1668 -136.226 1"
set_cam -169.185 -50.8064 -135.501 0 41.94 0 27.7676
say 2 "???" "Bonifaas? [wait 0.3]Are you alright?"
send_character_message 1 "set_head_target -172.836 -51.4568 -125.02 1"
send_character_message 2 "set_head_target -172.988 -50.9995 -122.478 1"
set_cam -173.73 -51.0129 -116.892 0 154.32 0 27.7676
send_character_message 1 "set_torso_target -174.567 -51.1742 -114.419 1"
set_character_pos 1 -175.292 -51.4541 -113.442 68
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\ghostsword_idle.anm\""
say 1 "Amos" "If the bastard I just stabbed was Bonifaas..."
send_character_message 1 "set_torso_target -174.567 -51.1742 -114.419 0"
send_character_message 1 "set_head_target -174.195 -50.5397 -118.341 1"
say 1 "Amos" "No. [wait 0.3]They're not alright."
send_character_message 2 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 2 "set_head_target -173.08 -50.293 -122.272 1"
send_character_message 2 "set_torso_target -170.364 -50.813 -136.315 1"
set_cam -169.202 -50.843 -135.066 0 34.3 0 27.7676
say 0 "???" "...!"
send_character_message 2 "set_animation \"Data\Animations\r_combatidle2.anm\""
set_cam -169.533 -50.8395 -138.206 0 162.66 0 20.73
say 2 "???" "Stop right there! [wait 0.3]What are you doing!?"
send_character_message 5 "set_animation \"Data\Animations\r_actionidle.anm\""
set_character_pos 5 -216.589 -53.9031 -27.2755 -42
send_character_message 1 "set_head_target -174.106 -50.954 -118.382 1"
set_cam -174.389 -50.8692 -116.373 0 162.02 0 20.73
say 0 "Amos" "..."
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\ghostsword_welcome.anm\""
send_character_message 1 "set_torso_target -174.404 -51.3705 -114.419 1"
send_character_message 1 "set_head_target -174.119 -50.7229 -118.352 1"
say 1 "Amos" "The slave named Edsel. [wait 0.5]Where is he?"
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\ghostsword_idle.anm\""
send_character_message 1 "set_torso_target -174.437 -51.1578 -114.373 1"
send_character_message 2 "set_torso_target -170.364 -50.813 -136.315 0"
send_character_message 2 "set_head_target -173.665 -51.2711 -121.131 1"
set_cam -175.449 -50.8142 -111.046 0 -8.81 0 20.73
say 2 "???" "I have no idea who you're talking about. [wait 0.3]Now stop!"
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\ghostsword_swordpoint.anm\""
send_character_message 1 "set_torso_target -175.382 -51.6487 -114.384 1"
say 1 "Amos" "I'm not going to stop until I'm out of here... [wait 0.3]with Edsel."
send_character_message 2 "set_torso_target -170.584 -51.3904 -136.388 1"
send_character_message 2 "set_head_target -173.806 -50.6793 -119.105 1"
set_cam -170.744 -50.9915 -133.706 0 -11.84 0 20.73
say 2 "???" "Back off!"
send_character_message 2 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_kneelfist.anm\""
set_character_pos 1 -196.145 -50.6446 -120.412 108
send_character_message 1 "set_eye_dir -197.048 -50.0582 -122.334 1"
send_character_message 2 "set_head_target -196.354 -50.2573 -120.75 1"
send_character_message 1 "set_head_target -197.021 -50.2049 -122.441 1"
send_character_message 2 "set_torso_target -197.074 -51.008 -121.987 1"
send_character_message 2 "set_eye_dir -196.94 -50.5199 -121.492 1"
set_character_pos 2 -197.147 -50.9 -122.651 -66
send_character_message 1 "set_torso_target -196.235 -50.8392 -121.354 0"
set_cam -173.447 -42.8399 -108.788 0 -27.64 0 20.73
say 0 "" "..."
set_character_pos 5 -339.375 -49.0643 -49.9234 -42
set_cam -196.079 -50.5088 -122.783 -0.4 176.01 0.55 20.73
say 1 "Amos" "...dammit."
send_character_message 2 "set_animation \"Data\Animations\r_dialogue_welcome.anm\""
set_cam -195.485 -50.1989 -120.958 0.68 46.28 -0.04 20.73
say 2 "???" "How do I get the explosives to end?"
send_character_message 2 "set_torso_target -197.267 -50.7291 -122.095 1"
send_character_message 2 "set_head_target -196.173 -50.423 -120.734 1"
send_character_message 2 "set_animation \"Data\Animations\r_actionidlethreat.anm\""
say 2 "???" "The slaves are going to escape!"
send_character_message 1 "set_head_target -197.047 -50.3764 -122.512 1"
send_character_message 1 "set_torso_target -196.384 -52.0515 -120.339 1"
set_cam -197.718 -50.4594 -122.399 -0.67 -138.51 0.1 20.73
say 1 "Amos" "They deserve to be free, [wait 0.3]you vermin."
send_character_message 2 "set_head_target -196.173 -50.1977 -120.734 1"
set_cam -195.479 -50.12 -123.039 0.36 107.45 0.57 20.73
say 2 "???" "Defiant to the very end..."
send_character_message 2 "set_head_target -196.173 -50.7501 -120.734 1"
say 2 "???" "Now die."
send_character_message 2 "set_torso_target -197.746 -50.8031 -122.293 1"
set_character_pos 2 -197.391 -50.9 -122.897 -119
send_character_message 2 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 4 "set_torso_target -207.429 -50.1905 -122.139 1"
send_character_message 4 "set_head_target -198.649 -50.1905 -122.549 1"
send_character_message 4 "set_eye_dir -206.612 -50.3812 -122.26 1"
send_character_message 4 "set_animation \"Data\Custom\timbles\therium-2\Animations\jairoidle.anm\""
set_character_pos 4 -208.162 -50.8258 -122.025 7
send_character_message 1 "set_eye_dir -195.813 -50.7611 -119.676 0.599266"
send_character_message 1 "set_head_target -196.006 -50.8658 -120.21 1"
send_character_message 2 "set_head_target -196.005 -50.5723 -119.983 1"
send_character_message 1 "set_torso_target -196.006 -50.9208 -120.304 1"
set_character_pos 1 -195.832 -50.6446 -119.492 108
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\woundedsit2.anm\""
set_cam -174.069 -42.8878 -107.809 0.05 -36.34 -0.67 20.73
say 0 "" "..."
set_cam -197.051 -50.6972 -123.113 -0.58 -160.88 0.34 24.0264
say 0 "Amos" "..."
send_character_message 2 "set_torso_target -197.747 -50.8012 -122.293 1"
send_character_message 2 "set_head_target -196.28 -50.414 -119.974 1"
set_cam -195.218 -50.1179 -123.445 0.42 101.22 0.53 19.3054
say 4 "????" "Bente! [wait 0.3]Over here! [wait 0.5]The kaicalk are rioting!"
set_cam -194.904 -50.0462 -123.609 -1.19 101.21 0.53 15.6529
send_character_message 2 "set_torso_target -198.159 -50.3159 -122.534 1"
send_character_message 2 "set_head_target -200.715 -50.2074 -122.757 1"
say 2 "Bente" "I'm coming, [wait 0.3]hold on!"
set_character_pos 5 -79.3593 -52.2802 -240.603 -42
send_character_message 1 "set_eye_dir -196.284 -50.5531 -119.883 0.599266"
send_character_message 1 "set_head_target -196.123 -50.7065 -120.318 1"
set_cam -198.26 -50.5615 -122.347 -0.69 -139.76 0.15 15.6529
say 0 "Amos" "..."
send_character_message 1 "set_head_target -196.092 -50.7874 -120.291 1"
send_character_message 1 "set_eye_dir -196.029 -50.6643 -119.726 0.599266"
say 0 "Amos" "..."
say 0 "" "You've completed the A-A path ending."
say 0 "" "For your Cutting Room Floor folder password... BETA TESTERS THIS DOESN'T APPLY TO YOU YA SAUCY SEXY BASTARDS"
send_character_message 1 "set_eye_dir -196.103 -50.857 -119.707 0.599266"
send_character_message 1 "set_head_target -196.075 -50.8604 -120.225 1"
say 0 "" "The second half is 'austere'."
say 0 "" "To get the first half, [wait 0.2]complete the B path ending."
say 0 "" "When you have both pieces, [wait 0.2]unlock the .zip in Data/TCRF"
send_character_message 1 "set_torso_target -195.991 -51.1273 -120.061 1"
send_character_message 1 "set_head_target -196.009 -50.8897 -119.948 1"
send_character_message 1 "set_eye_dir -196.103 -50.857 -119.707 0"
say 0 "T2-Dev" "Thank you for playing Therium-2."
send_level_message "go_to_main_menu"
