#name "Intro"
#participants 2

say 1 "Ghost" "Hey, Jairo-"
say 2 "Jairo" "Ghost, you're here. [wait 0.5]Finally."
say 2 "Jairo" "I located the bomb thru...[wait 1.0] word of mouth."
say 2 "Jairo" "It's at the end of this compound: [wait 0.5]near the lake."
say 2 "Jairo" "It'll be in a little green bag. [wait 0.5]Carry that with you."
say 1 "Ghost" "How'll I get out?"
say 2 "Jairo" "You either figure that out or you take them all out with you."
say 1 "Ghost" "Humbling..."
say 2 "Jairo" "Good luck. [wait 0.3]After this, it's straight to the tower."
say 1 "Ghost" "And straight back home, right?"
say 2 "Jairo" "Right."
