#name "Vada"
#participants 3

say 1 "Amos" "Hey, [wait 0.3]you have anything worth telling me?"
say 2 "Vada" "Maybe. [wait 0.5]I don't know."
say 2 "Vada" "I'm [wait 0.2]honestly surprised I could talk to you without [wait 0.3]screaming and running."
