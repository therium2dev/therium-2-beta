set_character_pos 1 267.483 -10.2701 -74.3903 0
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_armcross.anm\""
set_cam 269.078 -9.5371 -74.2803 0 86.51 0 40.8345
#name "Stop Editing!"
#participants 2

say 1 "Ghost" "Hey."
say 1 "Ghost" "You see this place?"
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_welcome.anm\""
send_character_message 1 "set_head_target 266.959 -9.56658 -75.0886 0.698301"
set_cam 266.348 -9.53711 -75.153 0 -112 0 40.8345
say 1 "Ghost" "You aren't supposed to be here - [wait 0.5]like, [wait 0.2]at all."
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_armcross.anm\""
say 1 "Ghost" "I understand going through the list of levels. [wait 0.3]Curiosity is good."
send_character_message 1 "set_head_target 266.958 -9.34803 -75.0886 0.698301"
say 1 "Ghost" "But to prematurely load a secret level?"
send_character_message 1 "set_head_target 268.381 -9.34803 -74.8323 0.698301"
set_cam 269.045 -9.60118 -75.1112 0.76 124.95 -0.49 40.8345
say 1 "Ghost" "Is this all you do with Therium-2? [wait 0.4]No?"
send_character_message 1 "set_torso_target 268.188 -9.76334 -76.2267 1"
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_handhips.anm\""
say 1 "Ghost" "Then why don't you play the actual game?"
send_character_message 1 "set_head_target 268.354 -9.12166 -74.8677 0.698301"
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_armcross.anm\""
send_character_message 1 "set_torso_target 268.188 -9.76334 -76.2267 0.253765"
say 1 "Ghost" "You could've spoiled the entire campaign for yourself!"
set_cam 267.84 -9.60519 -75.878 0.9 161.7 0.06 40.8345
send_character_message 1 "set_head_target 267.204 -9.54412 -75.5079 0.698301"
say 1 "Ghost" "Do you just not care about the storyline?"
send_character_message 1 "set_head_target 268.303 -9.9685 -74.608 0.698223"
set_cam 267.84 -9.60519 -75.878 1 161.31 0.07 40.8345
say 1 "Ghost" "Because if that's the case, then..."
send_character_message 1 "set_torso_target 268.115 -9.76334 -75.2677 1"
set_cam 267.84 -9.60553 -75.8784 1 161.31 0.07 40.8345
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_welcome.anm\""
send_character_message 1 "set_head_target 267.682 -9.37998 -75.2595 1"
say 1 "Ghost" "I dunno what to say!"
set_cam 267.879 -9.60576 -75.8768 1 161.31 0.07 40.8345
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_thoughtful.anm\""
send_character_message 1 "set_head_target 267.948 -9.37998 -74.8884 0.999983"
send_character_message 1 "set_torso_target 268.427 -9.76334 -74.5174 1"
say 1 "Ghost" "Just stop playing a story-based game, [wait 0.4]I guess?"
set_cam 268.392 -9.50651 -74.7315 0.78 118.71 -0.63 40.8345
send_character_message 1 "set_torso_target 268.224 -9.76334 -74.313 1"
send_character_message 1 "set_head_target 267.948 -9.39921 -74.8921 0"
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_facepalm.anm\""
say 0 "Ghost" "*sigh*"
send_character_message 1 "set_head_target -0.000396729 -0.000732422 0.000457764 0"
set_cam 268.345 -9.51681 -74.5809 1.24 105.09 1.81 40.8345
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_armcross.anm\""
say 1 "Ghost" "Just to teach you not to spoil the levels..."
say 1 "Ghost" "I'm gonna crash your game."
say 1 "Ghost" "Yeah. [wait 0.3]That's right. [wait 0.3]I'm gonna crash it, [wait 0.5]straight to your desktop."
say 1 "Ghost" "You could've saved yourself the POSSIBLE satisfaction..."
say 1 "Ghost" "Of finding this 'SUPER SECRET' level all on your own..."
say 1 "Ghost" "But no. [wait 0.3]You just had to load it."
say 1 "Ghost" "There's nothing even here for you, [wait 0.5]mate!"
say 1 "Ghost" "What were you expecting?"
say 1 "Ghost" "Alright, [wait 0.3]whatever."
say 1 "Ghost" "I'll crash your game now."
say 2 "Timbles" "CRASHING IN 3..."
say 2 "Timbles" "2..."
say 2 "Timbles" "1..."
set_character_pos 2 464.459 27.9161 -145.828 0
say 2 "Timbles" "[wait 0.5]Oh, [wait 0.4]darnit."
say 2 "Timbles" "Somehow the script didn't work.
say 2 "Timbles" "Nevermind, [wait 0.3]then. [wait 0.5]Carry on."
say 2 "Timbles" "Have fun wandering the vast emptiness of the super secret level."
say 2 "Timbles" "Spoilers: [wait 0.5]There's nothing here."
