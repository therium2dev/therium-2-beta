#name "ML-G"
#participants 1
set_dialogue_color 1 0 0.02 0.05
set_dialogue_voice 1 8
set_character_pos 1 -250.671 84.6179 118.415 -84
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 1 "set_eye_dir -250.51 85.4082 119.58 1"
send_character_message 1 "set_head_target -250.604 85.9171 119.517 1"
set_cam -254.259 85.1892 114.837 0 -142.46 0 66.0712
say 1 "Ghost" "..."
set_character_pos 1 -250.671 84.5053 118.415 -84
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\lanterntouch.anm\""
send_character_message 1 "set_head_target -250.604 85.9171 119.517 0"
say 1 "Ghost" "..."
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 1 "Ghost" "Well, [wait 0.3]at least the view's enjoyable."
send_level_message "loadlevel Data/Levels/t2/s2/a.xml"
