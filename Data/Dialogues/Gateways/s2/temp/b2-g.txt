#name "G"
#participants 1
set_character_pos 1 -184.507 8.65636 74.1511 -16
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 1 "set_eye_dir -183.257 9.66106 74.5451 1"
send_character_message 1 "set_head_target -183.373 10.4395 74.521 1"
set_cam -189.209 9.29771 75.5921 0 -76.77 0 70.4676
say 0 "Ghost" "..."
set_character_pos 1 -184.507 8.80057 74.1511 -16
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\lanterntouch.anm\""
send_character_message 1 "set_head_target -183.373 10.4395 74.521 0"
say 0 "Ghost" "..."
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 0 "Ghost" "Alright"
send_level_message "loadlevel Data/Levels/t2/s2/b2.xml"
