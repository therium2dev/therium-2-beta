#name "ML-ToB"
#participants 1
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
set_character_pos 1 107.871 -47.9299 253.993 -87
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 1 "set_eye_dir 107.948 -47.1462 255.217 1"
send_character_message 1 "set_head_target 107.923 -46.7691 255.09 1"
set_cam 103.731 -47.4096 248.854 0 -146.41 0 52.4723
say 1 "You" "..."
send_character_message 1 "set_head_target 107.923 -46.7691 255.09 0"
set_character_pos 1 107.871 -47.991 253.993 -87
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\lanterntouch.anm\""
say 1 "You" "..."
send_character_message 1 "set_animation \"Data\Animations\r_idle.anm\""
say 1 "You" "I hope I'm doing this right..."
send_level_message "loadlevel Data/Levels/t2/s1/b.xml"
