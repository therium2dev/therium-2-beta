#name "NL-A"
#participants 2
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
set_character_pos 1 -39.3623 -28.2602 -160.718 91
send_character_message 1 "set_head_target -39.4424 -26.4222 -162.026 0.968158"
set_cam -33.4389 -28.229 -152.099 0 35.05 0 40.1409
say 1 "Ghost" "..."
set_character_pos 1 -39.3623 -28.2753 -160.718 91
send_character_message 1 "set_animation \"Data/Animations/lanterntouch.anm\""
send_character_message 1 "set_head_target -39.4424 -26.4222 -162.026 0"
say 1 "Ghost" "..."
send_character_message 1 "set_animation \"Data/Animations/r_actionidle.anm\""
say 1 "Ghost" "I don't want to cause any more trouble here..."
send_level_message "loadlevel Data/Levels/t2/s1/a4.xml"
