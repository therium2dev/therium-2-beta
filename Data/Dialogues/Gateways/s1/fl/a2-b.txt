#name "FL-B"
#participants 1
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
set_character_pos 1 93.2651 -44.1739 155.678 -27
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 1 "set_eye_dir 94.4705 -43.5466 156.329 1"
send_character_message 1 "set_head_target 94.5854 -42.7011 156.362 1"
set_cam 90.6771 -43.8785 151.387 0 -145.53 0 61.6625
say 1 "Ghost" "..."
send_character_message 1 "set_head_target 94.5854 -42.7011 156.362 0"
set_character_pos 1 93.2651 -44.4722 155.678 -27
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\lanterntouch.anm\""
say 1 "Ghost" "..."
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 1 "Ghost" "Time to see where this goes."
send_level_message "loadlevel Data/Levels/t2/s1/b3.xml"
