#name "FL-G"
#participants 1
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
set_character_pos 1 -273.856 8.68819 -180.47 145
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 1 "set_eye_dir -274.726 10.0342 -181.16 1"
send_character_message 1 "set_head_target -274.55 9.76775 -181.049 1"
set_cam -268.764 9.16567 -180.672 0 89.14 0 65.8141
say 1 "Ghost" "..."
send_character_message 1 "set_head_target -274.55 9.76775 -181.049 0"
set_character_pos 1 -273.856 8.49173 -180.47 145
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\lanterntouch_lethal.anm\""
say 1 "Ghost" "..."
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 1 "Ghost" "Whatever the Cinderbreathe are doing here, [wait 0.3]they need to get out."
send_level_message "loadlevel Data/Levels/t2/s1/a4.xml"
