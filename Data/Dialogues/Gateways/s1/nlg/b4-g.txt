#name "NLG-G"
#participants 2
set_character_pos 1 49.9344 -34.3229 27.7385 -90
send_character_message 1 "set_head_target 49.895 -32.8023 29.0029 1"
set_cam 54.6672 -33.6335 22.7146 0 141.19 0 51.4503
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
say 1 "Ghost" "..."
set_character_pos 1 49.9344 -34.365 27.7385 -90
send_character_message 1 "set_animation \"Data/Animations/lanterntouch.anm\""
send_character_message 1 "set_head_target 49.895 -32.8023 29.0029 0"
say 1 "Ghost" "..."
send_character_message 1 "set_animation \"Data/Animations/r_actionidle.anm\""
say 1 "Ghost" "What is this cave for... [wait 0.5]some ancient slave trade, [wait 0.3]maybe...?"
send_level_message "loadlevel Data/Levels/t2/s1/b5.xml"
