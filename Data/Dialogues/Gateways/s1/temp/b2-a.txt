#name "-A"
#participants 1
set_character_pos 1 -129.972 29.9862 152.472 -180
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
send_character_message 1 "set_eye_dir -131.479 30.7281 152.489 1"
send_character_message 1 "set_head_target -131.442 31.3102 152.439 1"
set_cam -125.728 30.9165 155.1 0 62.56 0 62.488
say 0 "Ghost" "..."
set_character_pos 1 -129.972 29.8999 152.472 -180
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\lanterntouch.anm\""
send_character_message 1 "set_head_target -131.442 31.3102 152.439 0"
say 0 "Ghost" "..."
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 0 "Ghost" "Alright."
send_level_message "loadlevel Data/Levels/t2/s1/a3.xml"
