#name "Gateway2"
#participants 1
set_character_pos 1 81.8774 -60.2826 339.766 -79
send_character_message 1 "set_eye_dir 82.1244 -59.3782 341.043 1"
send_character_message 1 "set_head_target 82.106 -59.2037 340.965 1"
set_cam 84.7139 -59.8725 343.579 0 40.21 0 33.7878
say 0 "Ghost" "..."
set_character_pos 1 81.8774 -60.4481 339.766 -79
send_character_message 1 "set_animation \"Data\Custom\timbles\therium-2\Animations\lanterntouch.anm\""
send_character_message 1 "set_head_target 82.106 -59.2037 340.965 0"
say 0 "Ghost" "..."
send_character_message 1 "set_animation \"Data\Animations\r_actionidle.anm\""
say 0 "Ghost" "Alright."
send_level_message "loadlevel Data/Levels/t2/s1/a2.xml"
