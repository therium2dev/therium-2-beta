#name "Ano"
#participants 3
set_dialogue_color 1 0 0.52 0.95
set_dialogue_voice 1 8
set_dialogue_color 2 0.9 0.9 0
set_dialogue_voice 2 7
say 2 "Ano" "Amos, [wait 0.3]you fruit. [wait 0.5]Why are you here?"
say 1 "Ghost" "Hey, [wait 0.3]my name's not Amos. [wait 0.5]It's Ghost."
say 2 "Ano" "The adoption of a nickname is a somewhat important part of your character arc, [wait 0.3]and you're just going to throw that away?"
say 1 "Ghost" "[wait 0.3]Sure. [wait 0.5]Doesn't sound as cool."
say 2 "Ano" "Well, [wait 0.3]do you want to know Gunk's real name?"
say 1 "Ghost" "Nah. [wait 0.5]'Gunk' sounds cooler."
say 2 "Ano" "[wait 0.3]This is why I'm a grumpy old man..."
