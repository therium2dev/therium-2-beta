# Therium-2 #

## How to Download ##
I recommend you use the [Steam Workshop version](http://steamcommunity.com/sharedfiles/filedetails/?id=1126025778), as it can auto-update every time I push something.


### Local (ModDB) Version ###

When you download the mod from ModDB, just copy the folder named Therium-2 out of your download into your Overgrowth installation directory.

Your Overgrowth installation directory is, by default, either;

* Steam/steamapps/common/Overgrowth
* Program Files (x86)/Wolfire/Overgrowth
* Program Files/Wolfire/Overgrowth

Navigate to *Overgrowth/Data/Mods*, and paste Therium-2 there.


### Steam Workshop Version ###

This is far more simple, and automatically updates when patches and content is released.

Just click [this link](http://steamcommunity.com/sharedfiles/filedetails/?id=1126025778), log in to your Steam account and click Subscribe. Steam will automatically download Therium-2.



### How to Navigate to your Therium-2 directory with the Steam Workshop ###
*Note: These instructions are not necessary if you downloaded Therium-2 from ModDB.*

Go to your Steam directory. By default, this is located in either:

	Program Files (x86)/Steam
	Program Files/Steam

Navigate to *Steam/steamapps/workshop/content/25000/1126025778*

