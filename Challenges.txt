== THERIUM-2 CHALLENGES ==

Here are some challenges that you can do on each level to make the game more challenging, or have more replay value. I do not expect all of these challenges to be possible or even fun.


NO INJURIES			Don't get damaged once throughout the level.

NO KILLS			Keep everyone on the level alive. Knocking guards unconscious is allowed.

NON-LETHAL GHOST	Keep everyone on the level alive and conscious.

FULL LETHALITY		Kill everyone on the level.

COMBAT ONLY			You must engage enemies in combat.

ALL ELIMINATIONS	When possible, defeat every single enemy in the level.

ALL NOTES			Find and read every single note in the level. Includes ash inscriptions in S2 levels.

ALL DIALOGUES		Encounter every unique dialogue in the level.

NO BLOCKING			You can't block enemy attacks; you can only dodge and take damage.

NO DODGING			You can't dodge enemy attacks; you can only block and take damage.

NO FIRST STRIKES	The enemy has to attack you first before you attack them.

NO DISARMS			Don't disarm any enemy for any reason.

NO ALERTS			No enemies can enter combat with you.

INVISBILITY			You can't be seen once, even if the enemy hasn't entered combat.

UNDETECTED			You can't be seen or heard once, even if the enemy hasn't entered combat.

BRUTAL MODE			Set game_difficulty to 2.5 in your configuration file.

DISTRACTION			Get every single enemy to move from their patrol or position at least once by distracting them.

DIVIDE AND CONQUER	Split off every single enemy before elimination. Only one at a time.

BESET ON ALL SIDES	Eliminate every single enemy on the map in only one combat encounter.

DUCK				Complete the level only crouching.

BROKEN LEG			Complete the level jumping as little as possible.

THE GRIND			In combat, you can only eliminate enemies by judo throwing.

ALPHA STYLE			In combat, you can only eliminate enemies by dropkicking.

WHIPLASH			In combat, you can only eliminate enemies by throwing your weapons.

FLOOR IS LAVA		Touch the terrain as little as possible. Only touch objects.

HOPSCOTCH			Every single object you touch has to be touched an even amount of times.

SPECIFICITY			Every single object you touch has to be touched an increasing amount of times (first object you touch = 1, second object = 2, etc).

RITUAL				Bring every enemy's *conscious* corpse to one of the secret decals in a circle formation. When in formation, kill them all.

SAVING FOR LATER	Any enemies knocked unconscious have to be killed afterwards.

CRUEL AND UNUSUAL	All enemies have to be eliminated with falling damage.

ON SITE TRICKERY	All enemies have to be eliminated in combat with environmental damage.

CIRCUS				Every single time you jump, you have to flip.



- PRESET CHALLENGES -

FLAWLESS COMBAT
No Injuries, No Kills, Combat Only, No First Strikes, All Eliminations, Non-Lethal

FLAWLESS STEALTH
No Kills, Undetected, No Alerts, Divide And Conquer, All Eliminations

JOKER
Combat Only, All Eliminations, No Blocking, No First Strikes, Distraction, Duck, Broken Leg, Non-Lethal

PARANOID
No Injuries, Floor Is Lava, Hopscotch, Full Lethality, Divide And Conquer

TRUE GHOST
No Injuries, All Notes, All Dialogues, No Disarms
